import numpy as np
from Modules.cost_function import CostFunction
import time


# Class MaxCov for evaluating greedy algorithm for Max Coverage Problem
class MaxCov:
    # Initializing function
    def __init__(self, env, num_of_phi=5, num_of_theta=3, type_list=None):
        self.environment = env
        self.num_of_phi = num_of_phi
        self.num_of_theta = num_of_theta
        if type_list is None:
            self.type_list = ['Ultrasound', 'Camera']
        else:
            self.type_list = type_list

    # Greedy algorithm
    # num_of_phi - number of phi (horizontal angle) which sensor can be oriented
    # num_of_theta - number of theta (vertical angle) which sensor can be oriented
    # type_list - list of sensor types which can be placed
    def compute(self):

        # Hardcoded prices
        sensors_prices = {'Ultrasound': 25, 'Camera': 70, 'Radar': 100, 'Lidar': 1500}
        full_price = 0
        
        # Print initial message
        print('Greedy algorithm for Max Coverage problem starts...')
        type_str = ''
        for s_type in self.type_list:
            type_str += s_type + ', '
        type_str = type_str[:-2]
        print('Algorithm settings: number of horizontal angles: {}, number of vertical angles: {},'
              ' list of sensors type: {}.'.format(self.num_of_phi, self.num_of_theta, type_str))
        # Set of sensors in all possible positions and orientations
        start = time.time()
        possible_list = CostFunction(self.environment).get_dispositions(self.num_of_phi, self.num_of_theta,
                                                                        self.type_list)
        end = time.time()
        print('Time requested by all evaluating {}.\n'.format(time.strftime("%H:%M:%S", time.gmtime(end - start))))

        # Initialization of loop
        final_mask = np.full_like(self.environment.full_grid[:, 0], dtype=bool, fill_value=False)
        result_1 = []
        result_2 = []
        covers = []
        rel_covers = []
        while full_price <= self.environment.budget:
            # Evaluating of ratio cover to prices
            for sensor in possible_list:
                mask = final_mask | sensor[3]
                rel_cover = np.sum(self.environment.full_grid[:, 3][mask]) / sensors_prices[sensor[2]]
                rel_covers.append(rel_cover)
            # Choose and add best sensor
            max_index_1 = np.argmax(rel_covers)
            ans_1 = possible_list[max_index_1]
            if full_price + sensors_prices[ans_1[2]] <= self.environment.budget:
                final_mask = final_mask | ans_1[3]
                full_price += sensors_prices[ans_1[2]]
                result_1.append(ans_1)
                rel_covers = []
            else:
                full_price += sensors_prices[ans_1[2]]
        m_cover_1 = np.sum(self.environment.full_grid[:, 3][final_mask])

        # Find maximum cover of particular sensor
        for sensor in possible_list:
            mask = sensor[3]
            cover = np.sum(self.environment.full_grid[:, 3][mask])
            covers.append(cover)
        max_index_2 = np.argmax(covers)
        ans_2 = possible_list[max_index_2]
        result_2.append(ans_2)
        sens = self.environment.init_sensor(ans_2[2], ans_2[0], ans_2[1])
        self.environment.add_sensor(sens, 0)
        m_cover_2 = np.sum(self.environment.full_grid[:, 3][ans_2[3]])
        self.environment.remove_all_sensors()

        if m_cover_1 > m_cover_2:
            result = result_1
        else:
            result = result_2
        # Build solution
        for index, sensor in enumerate(result):
            sens = self.environment.init_sensor(sensor[2], sensor[0], sensor[1])
            self.environment.add_sensor(sens, index)

        # Print solution
        self.environment.print_setup()
        self.environment.plot_grid()
        self.environment.remove_all_sensors()
