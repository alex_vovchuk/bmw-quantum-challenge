import time
import numpy as np
from braket.circuits import Circuit
from braket.circuits.observable import Observable
import itertools
import matplotlib.pyplot as plt
from matplotlib import cm
from braket.devices import LocalSimulator

from Modules.cost_function import CostFunction
from scipy import optimize
import copy


class QAOA:
    # Function to initializing class instance
    def __init__(self, environment, num_of_orient, list_of_types):
        self.environment = environment
        self.number_of_sensors = len(self.environment.sensors)
        self.number_of_orient = num_of_orient
        self.qubits_per_sensor = len(bin(num_of_orient)[2:])
        self.list_of_types = list_of_types
        self.pauli_gates = None
        self.j_sens = CostFunction(self.environment).get_weights(self.number_of_orient, self.list_of_types)
        self.j_over_sens = CostFunction(self.environment).get_overlap_weights(self.number_of_orient, self.list_of_types)

    # Function to create a bracket (1 ± sigma) where sigma - pauli operator.
    # n_sensor - number of sensor which involved in interaction
    # n_type - number of types of the sensor
    # n_q - number of a qubit in the particular sensor
    # state - 0 or 1 (on the value of a state depends which a sign take in the bracket)
    # return a nested list with two lists, first represent 1 and second -  a pauli operator (each of them - also a list,
    # where the first element - a coefficient, the second - a boolean list, where True value represents on which qubit a
    # Pauli operator acts)
    def create_multiplier(self, n_sensor,  n_q, state, add=0):
        # Calculating a number of qubits for representing types
        n_type_q = self.qubit_per_type()
        # Initializing operators
        i = np.zeros(self.number_of_sensors * (self.qubits_per_sensor + n_type_q) + add, dtype=bool)
        z = np.zeros(self.number_of_sensors * (self.qubits_per_sensor + n_type_q) + add, dtype=bool)
        # Calculate on which qubit a Pauli operator acts
        index = (n_sensor - 1) * (self.qubits_per_sensor + n_type_q) + n_q + add
        z[index] = True
        # Choose a sign before Pauli operators
        if state == 0:
            return [[1/2, i], [1/2, z]]
        elif state == 1:
            return [[1/2, i], [-1/2, z]]

    # Function, calculating a number of qubits needed to represents a type
    # return integer
    def qubit_per_type(self):
        n_type = len(self.list_of_types)
        if n_type == 1:
            n_type_q = 0
        else:
            n_type_q = len(bin(n_type - 1)[2:])
        return n_type_q

    # Function, that calculate the product of a different (1 ± sigma) bracket
    # list_of_brackets - a list of brackets
    # j - a multiplier near brackets
    # return a list with all additions
    @staticmethod
    def multiply_by(list_of_brackets, j):
        # Check a number of brackets, if it only one - return by itself
        if len(list_of_brackets) == 1:
            list_of_brackets[0][0] = j
            return list_of_brackets
        else:
            bracket_1 = list_of_brackets[0]
            # Loop over all brackets except first
            for bracket_index in range(1, len(list_of_brackets)):
                result = []
                # Loop for calculating all combination of brackets
                for element_1, element_2 in itertools.product(bracket_1, list_of_brackets[bracket_index]):
                    res = [0, 0]
                    # Adding boolean mask with 'and' which represent additions with more that one paulis operators and
                    # calculating sign of coefficients
                    res[1] = element_1[1] | element_2[1]
                    res[0] = element_1[0] * element_2[0]
                    result.append(res)
                bracket_1 = result
            # Adding coefficient
            for addition in bracket_1:
                addition[0] = addition[0] * j
            return bracket_1

    # Function that adds lists of operators, finding same operators and adding coefficients
    # list_of_operators - list of operators which will add
    # return list of operators
    @staticmethod
    def add(list_of_operators):
        result = []
        # Outer loop for checking all elements
        for index, addition in enumerate(list_of_operators):
            iter = 0
            # Inner loop for taking elements for compare
            for i in range(index + 1, len(list_of_operators)):
                if np.all(addition[1] == list_of_operators[i][1]):
                    # If operators the same - adding coefficients to second elements and break the loop
                    list_of_operators[i] = [addition[0] + list_of_operators[i][0], addition[1]]
                    iter += 1
                    break
            # If any of elements not the same - take to final result
            if iter == 0:
                result.append(addition)
        return result

    # Function to get the first part of the Hamiltonian (without overlap)
    # return list of pauli operators
    def get_hamiltonian_1(self, add=0):
        # j_sens - classically determined areas (volumes) covered by each sensor in each orientation.
        j_sens = self.j_sens
        result = []
        # Check each sensor
        list_of_types = list(j_sens.keys())
        for n in range(self.number_of_sensors):
            # Check each type
            for s_type in list_of_types:
                j_sens_type = j_sens[s_type]
                list_for_js = []
                # Check each orientation
                for key in j_sens_type.keys():
                    if j_sens_type[key][n] != 0:
                        list_for_paulis = []
                        # Creating a list of pauli operators
                        for index, state in enumerate(key):
                            pauli = self.create_multiplier(n + 1, index, int(state), add)
                            list_for_paulis.append(pauli)
                        # Multiply all brackets
                        js = self.multiply_by(list_for_paulis, j_sens_type[key][n])
                        list_for_js.extend(js)
                    else:
                        pass
                # Construct a bracket for a type representation
                if len(list_of_types) == 1:
                    pass
                else:
                    len_of_str = len(bin(len(list_of_types) - 1)[2:])   # Lens of a string, that represent a type
                    states = bin(list_of_types.index(s_type))[2:].zfill(len_of_str)      # State for a particular type

                    # Creating multipliers
                    for index, state in enumerate(states):
                        type_bracket = self.create_multiplier(n + 1, self.qubits_per_sensor + index, int(state), add)
                        s = [list_for_js, type_bracket]
                        list_for_js = self.multiply_by(s, 1)
                # Adding pauli operators for all sensors
                n_result = self.add(list_for_js)
                result.extend(n_result)
        result = self.add(result)
        return result

    # Function to get the second part of the Hamiltonian (overlaps)
    # return list of pauli operators
    def get_hamiltonian_2(self, add=0):
        # j_over_sens - classically determined areas (volumes) covered by each sensor in each orientation.
        j_over_sens = self.j_over_sens
        result = []
        list_of_pair_of_types = list(j_over_sens.keys())
        # Creating a list with all types
        types = []
        for pair in list_of_pair_of_types:
            x, y = pair.split('/')
            types.append(x)
            types.append(y)
        types = list(set(types))
        # Check each pair of sensor
        for n in range(self.number_of_sensors):
            for m in range(self.number_of_sensors):
                if n == m:
                    pass
                else:
                    # Check all pairs of types
                    for s_type in list_of_pair_of_types:
                        j_over_sens_type = j_over_sens[s_type]
                        list_for_js = []
                        # Check each pair of orientation
                        for key in j_over_sens_type.keys():
                            if j_over_sens_type[key][n][m] != 0:
                                list_for_paulis = []
                                # Creating a list of pauli operators
                                for index, state in enumerate(key[:int(len(key) / 2)]):
                                    pauli = self.create_multiplier(n + 1, index, int(state), add)
                                    list_for_paulis.append(pauli)
                                for index, state in enumerate(key[int(len(key) / 2):]):
                                    pauli = self.create_multiplier(m + 1, index, int(state), add)
                                    list_for_paulis.append(pauli)
                                # Multiply all brackets
                                js = self.multiply_by(list_for_paulis, - j_over_sens_type[key][n][m] / 2)
                                list_for_js.extend(js)
                            else:
                                pass
                        # Construct bracket for a type representation
                        if len(list_of_pair_of_types) == 1 or len(list_for_js) == 0:
                            pass
                        else:
                            len_of_str = len(bin(len(types) - 1)[2:])  # Lens of string, that represent one type
                            first, second = s_type.split('/')
                            states_1 = bin(types.index(first))[2:].zfill(len_of_str)  # State for particular type
                            states_2 = bin(types.index(second))[2:].zfill(len_of_str)  # State for particular type
                            # Creating multiplier
                            for index, state in enumerate(states_1):
                                type_bracket = self.create_multiplier(n + 1,
                                                                      self.qubits_per_sensor + index, int(state), add)
                                s = [list_for_js, type_bracket]
                                list_for_js = self.multiply_by(s, 1)
                            for index, state in enumerate(states_2):
                                type_bracket = self.create_multiplier(m + 1,
                                                                      self.qubits_per_sensor + index, int(state), add)
                                s = [list_for_js, type_bracket]
                                list_for_js = self.multiply_by(s, 1)
                        # Adding pauli operators for all sensors
                        n_result = self.add(list_for_js)
                        result.extend(n_result)
        result = self.add(result)
        return result

    # Function that add two parts of cost hamiltonian
    def create_layer(self, add=0, normalization=False):
        if add == 1:
            bracket = np.zeros(self.number_of_sensors * (self.qubits_per_sensor + self.qubit_per_type()) + add,
                               dtype=bool)
            bracket[0] = True
            bracket = [1, bracket]
            pauli_gates = self.add(self.get_hamiltonian_2(add) + self.get_hamiltonian_1(add))
            s = [pauli_gates, [bracket]]
            pauli_gates = self.multiply_by(s, 1)
        else:
            pauli_gates = self.add(self.get_hamiltonian_2(add) + self.get_hamiltonian_1(add))
        if normalization:
            # Normalization coefficients to 1
            max_coof = - np.inf
            for pauli_gate in pauli_gates:
                if np.abs(pauli_gate[0]) > max_coof and np.any(pauli_gate[1]):
                    max_coof = np.abs(pauli_gate[0])
            for pauli_gate in pauli_gates:
                pauli_gate[0] = pauli_gate[0] / max_coof
        return pauli_gates

    # Function to create circuit of full hamiltonian
    # pauli_gates - a list of operators, that represent hamiltonian
    # gamma - outer parameter of QAOA
    # return Braket Circuit
    @staticmethod
    def hamiltonian(pauli_gates, gamma):
        circ = Circuit()
        # Check each operator
        for gate in pauli_gates:
            indexes = [i for i, x in enumerate(gate[1]) if x]
            # Add RZ operator if pauli acts on a one qubit
            if len(indexes) == 1:
                circ.rz(indexes[0], 2*gamma*gate[0])
            elif len(indexes) == 0:
                pass
            else:
                # Add pauli operators with CNOT and RZ operators
                for i in indexes[:-1]:
                    circ.cnot(i, indexes[-1])
                circ.rz(indexes[-1], 2*gamma*gate[0])
                for i in indexes[:-1][::-1]:
                    circ.cnot(i, indexes[-1])
        return circ

    # Function to implement evolution with mixer Hamiltonian
    # beta - outer parameter of QAOA
    # return Braket Circuit
    def mixer(self, beta):
        circ = Circuit()
        n_type_q = self.qubit_per_type()
        # Adding RX operators to all qubits
        for qubit in range(self.number_of_sensors * (self.qubits_per_sensor + n_type_q)):
            gate = Circuit().rx(qubit, 2 * beta)
            circ.add(gate)
        return circ

    # Function that create a full circuit with cost hamiltonian and mixer
    # pauli_gates - a list of operators, that represent hamiltonian
    # params - list of betas and gammas, outer parameters of QAOA
    # return Braket Circuit
    def create_qaoa_circuit(self, pauli_gates, params):
        circ = Circuit()
        # Adding hadamard operators to all qubits for creating superposition state
        n_type_q = self.qubit_per_type()
        h_on_all = Circuit().h(range(0, self.number_of_sensors * (self.qubits_per_sensor + n_type_q)))
        circ.add(h_on_all)
        # Adding hamiltonian and mixer p times, where - deep of QAOA
        for i in range(0, len(params[::2])):
            circ.add(self.hamiltonian(pauli_gates, params[i]))
            circ.add(self.mixer(params[i + len(params[::2])]))
        return circ

    def plot_params(self):
        fig = plt.figure(figsize=(14, 8), dpi=80)
        ax = fig.add_subplot(projection='3d')
        x = np.arange(0, 4 * 2 * np.pi, 0.25)
        y = np.arange(0, np.pi, 0.2)
        xs, ys = np.meshgrid(x, y)
        zs = np.empty((len(x), len(y)))
        self.pauli_gates = self.create_layer(normalization=True)
        for i in range(len(x)):
            for j in range(len(y)):
                zs[i][j] = self.compute_params([xs[j][i], ys[j][i]])
        ax.plot_surface(xs, ys, zs.T, cmap=cm.coolwarm)
        plt.show()

    # Function that classically optimize parameters bates and gammas
    # params - a list of betas and gammas
    # return optimized parameters
    def optimize(self, params, optimizer):
        if optimizer == 'brute':
            ranges = self.constraints(params)
            return optimize.brute(self.compute_params, ranges, Ns=10)
        elif optimizer == 'COBYLA':
            result = optimize.minimize(self.compute_params, params, constraints={'type': 'ineq',
                                                                                 'fun': self.cob_constraints},
                                       method='COBYLA', options={'rhobeg': 0.75})
            print('Details of optimization: \n {}'.format(result))
            return result.x

    # Function that builds constraints for COBYLA optimization
    # params - betas/gammas parameters
    # return array with constraints
    @staticmethod
    def cob_constraints(params):
        cons = []
        for i in range(len(params)):
            if i < len(params) / 2:
                cons.extend([- (params[i] - 2 * np.pi) * params[i]])
            else:
                cons.extend([- (params[i] - np.pi) * params[i]])
        cons_array = np.array(cons)
        return cons_array

    # Function that builds constraints for brute optimizer
    # params - betas/gammas parameters
    # return array with constraints
    @staticmethod
    def constraints(params):
        cons = []
        for i in range(len(params)):
            if i < len(params) / 2:
                cons.append((0, 2 * np.pi))
            else:
                cons.append((0, np.pi))
        return tuple(cons)

    # Function that compute with a local simulator a mean value of the cost function in a state with particular
    # parameters betas and gammas
    # params - a list of betas and gammas
    # return a mean value
    def compute_params(self, params):
        device = LocalSimulator()
        circ = self.create_qaoa_circuit(self.pauli_gates, params)
        # print(circ)
        # print(params)
        result = device.run(circ, shots=1000).result()
        counts = result.measurement_counts
        init_sensors = copy.deepcopy(self.environment.sensors)
        self.build_result(counts)
        mean = CostFunction(self.environment).get_effective_volume()
        # print(mean)
        # print(max(counts, key=counts.get))
        # self.environment.plot_grid()
        self.environment.sensors = init_sensors
        return - mean

    #  Function that compute a final result of QAOA
    # p - number of iterations of QAOA
    def compute_qaoa(self, p, optimizer, device=LocalSimulator(), s3_location=None):
        device = device
        params = [1 for _ in range(2 * p)]
        print('Building the Hamiltonian...', end='')
        self.pauli_gates = self.create_layer(normalization=True)
        print('Done.')
        print("Optimization of QAOA parameters Gammas and Betas...", end='')
        start = time.time()
        opt_params = self.optimize(params, optimizer)
        end = time.time()
        print('Done.')
        print('Time requested for optimization {}.'.format(time.strftime("%H:%M:%S", time.gmtime(end - start))))
        print('Optimal parameters: {}'.format(opt_params))
        circ = self.create_qaoa_circuit(self.pauli_gates, opt_params)
        # print(circ)
        print('Depth of circuit {}'.format(circ.depth))
        if s3_location:
            result = device.run(circ, s3_location, shots=1000).result()
        else:
            result = device.run(circ, shots=10000).result()
        all_counts = result.measurement_counts
        counts = {}
        for key in all_counts.keys():
            if all_counts[key] > 5:
                counts[key] = all_counts[key]
        self.plot_histogram(counts)
        print('Best solution: {}'.format(max(counts, key=counts.get)))
        self.build_result(counts)
        self.environment.print_setup()
        self.environment.plot_grid()
        self.environment.remove_all_sensors()

    # Function that plot histogram for counts of measurement
    @staticmethod
    def plot_histogram(counts):
        fig = plt.figure(figsize=(14, 8), dpi=80)
        ax = fig.add_subplot()
        ax.bar(counts.keys(), counts.values())
        ax.set_xlabel('Bitstrings')
        ax.set_ylabel('Counts')
        plt.xticks(rotation=90)
        plt.show()

    # Function that change environment according to quantum results
    def build_result(self, counts):
        # Choose best solution
        answer = max(counts, key=counts.get)
        # Split bitstring for each sensors
        sensors = [answer[i:i + self.qubits_per_sensor + self.qubit_per_type()] for i in range(0, len(answer),
                                                                                               self.qubits_per_sensor +
                                                                                               self.qubit_per_type())]
        fin = []
        # State extraction from corresponding bit string
        for index, sensor in enumerate(sensors):
            s_angle = sensor[:self.qubits_per_sensor]
            s_type = sensor[self.qubits_per_sensor:]
            if s_type == '':
                new_sens = self.environment.init_sensor(self.list_of_types[0])
            else:
                new_sens = self.environment.init_sensor(self.list_of_types[int(s_type, 2)])
            angle_step = (180. - new_sens.horizontal_angle) / (self.number_of_orient - 1.)
            pos, orient = self.environment.sensors[index].position, self.environment.sensors[index].orientation
            if int(s_angle, 2) == 0 or int(s_angle, 2) > self.number_of_orient:
                pass
            else:
                orient[0] = orient[0] - 90 + new_sens.horizontal_angle / 2 + (int(s_angle, 2) - 1) * angle_step
                new_sens.change_orientation(orient)
                new_sens.change_position(pos)
                fin.append(new_sens)
        self.environment.remove_all_sensors()
        # Adding new sensors
        for index, sens in enumerate(fin):
            self.environment.add_sensor(sens, index)


# Class for estimation of the ground state
class EstimGState(QAOA):

    def __init__(self, environment, num_of_orient, list_of_types, step, c):
        QAOA.__init__(self, environment, num_of_orient, list_of_types)
        self.step = step
        self.c = c
        self.j_sens = CostFunction(self.environment).get_weights(self.number_of_orient, self.list_of_types)
        self.j_over_sens = CostFunction(self.environment).get_overlap_weights(self.number_of_orient, self.list_of_types)

    # Function for calculating average of sigma Z of zero qubit
    def compute_circ(self, device=LocalSimulator(), s3_location=None):
        device = device
        n = 16 * self.step + 1
        alphas = np.linspace(- 8 * np.pi, 8 * np.pi, n)
        shots = 1000
        results = []
        for alpha in alphas:
            circ = self.create_circuit(alpha)
            if s3_location:
                result = device.run(circ, s3_location, shots=shots).result()
            else:
                result = device.run(circ, shots=shots).result()
            value = result.result_types[0].value
            zeros = list(value).count(-1) / shots
            ones = list(value).count(1) / shots
            results.append(ones - zeros)
        return results

    # Function for creating circuit
    def create_circuit(self, alpha):
        circ = Circuit()
        # Adding hadamard operators to all qubits for creating superposition state
        n_type_q = self.qubit_per_type()
        h_on_all = Circuit().h(range(0, self.number_of_sensors * (self.qubits_per_sensor + n_type_q) + 1))
        circ.add(h_on_all)
        u = self.create_layer(add=1, normalization=True)
        h = self.hamiltonian(u, alpha)
        circ.add(h)
        circ.rz(0, - self.c * alpha).ry(0, - np.pi / 2).sample(observable=Observable.Z(), target=0)
        return circ

    # Plotting of Fourier Discrete Transform of average sigma Z
    def fourier_trans(self, m):
        a = self.compute_circ()
        n = 16 * self.step + 1
        t = np.linspace(- 8 * np.pi, 8 * np.pi, n)
        aw = list()
        w = list()
        for j in np.arange(- m * np.pi, m * np.pi + np.pi / 120., np.pi / 120.):
            w.append(j)
            tmp = 0
            for i in range(len(a)):
                tmp += a[i] * np.cos(j * t[i])
            aw.append(tmp)
        fig = plt.figure(figsize=(14, 8), dpi=80)
        ax1 = fig.add_subplot()
        ax1.plot(w, aw)
        ax1.set_xticks(np.arange(- int(m * np.pi), int(m * np.pi), 1))
        ax1.grid(axis='x', linestyle='--', linewidth=0.5)
        ax1.set_ylabel("$A(\omega)$")
        ax1.set_xlabel("$\omega$")
        plt.show()
