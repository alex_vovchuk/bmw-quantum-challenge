from Modules.allowed_sensor_position import build_position_grid


# sensor with parameters:
# type - string type of sensor (camera, ultrasound, radar or lidar);
# position - numpy array with Cartesian coordinates (x, y, z) of sensor;
# orientation - numpy array with spherical coordinates (phi, theta) of points which belong
# to central line in sensors field of view;
# allowed_positions - dictionary with keys - names of rectangle areas on the car where sensor can be placed and values -
# grid for specific rectangle area;
# s_range - range of sensors field of view;
# horizontal_angle - horizontal angle of view;
# vertical_angle - vertical angle of view;
# price - price of sensor;
# polygon - specific rectangle area in which sensor placed (optional)
# mask - mask of sensor covered zone
class Sensor:
    # function to create sensor of given type and with given position and orientation,
    # if sensor type is None and all other sensors parameters are present function returns object of class Sensor
    # with this parameters,
    # if function has only one sensors parameter (its type) than it returns object of class Sensor
    # with default parameters for this sensors type
    # pos_step - step (distance between two nearest greed points) for allowed position grid
    def __init__(self, position, orientation, s_type, pos_step=0.1):
        # create sensor when sensors type is None
        self.type = s_type
        self.position = position
        self.orientation = orientation
        self.allowed_position = build_position_grid(s_type, pos_step)
        self.polygon = None
        self.mask = None

        # create sensor when its type is not None
        if s_type is not None:
            if s_type == 'Ultrasound':
                # default parameters for ultrasound
                self.s_range = 5
                self.horizontal_angle = 60
                self.vertical_angle = 30
                self.price = 25

            elif s_type == 'Camera':
                # default parameters for camera
                self.s_range = 8
                self.horizontal_angle = 80
                self.vertical_angle = 80
                self.price = 70

            elif s_type == 'Radar':
                # default parameters for radar
                self.s_range = 80
                self.horizontal_angle = 120
                self.vertical_angle = 30
                self.price = 100

            elif s_type == 'Lidar':
                # default parameters for lidar
                self.s_range = 200
                self.horizontal_angle = 90
                self.vertical_angle = 25
                self.price = 1500

    # function to change sensors position
    # new_position - numpy array with Cartesian coordinates (x, y, z)
    # function replaces old values of sensors coordinates with new values from new_position
    def change_position(self, new_position):
        self.position = new_position

    # function to change sensors orientation
    # new_orientation - numpy array with spherical coordinates (phi, theta)
    # function replaces old values of spherical coordinates which represent sensors orientation
    # with new values from new_orientation
    def change_orientation(self, new_orientation):
        self.orientation = new_orientation
