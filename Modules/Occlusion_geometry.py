import numpy as np


# function to create full grid with coordinates of points on all regions from occlusion_geometry dataframe;
# step - difference of x, y or z coordinates of two nearest points of some region;
# full_grid - two-dimensional numpy array with 3 columns (x, y, z coordinates of points and values of critical indexes
# at these points (always 0)) and n rows (number of points which belong to all regions of cars surface);
# function returns full_grid

def get_occlusion(step):
    step = step * 100
    full_grid = get_right_side_occlusion(step)
    full_grid = get_left_side_occlusion(full_grid, step)
    full_grid = get_front_occlusion(full_grid, step)
    full_grid = get_back_occlusion(full_grid, step)
    full_grid = get_windshield_occlusion(full_grid, step)
    full_grid = get_bonnet_occlusion(full_grid, step)
    full_grid = get_front_roof_occlusion(full_grid, step)
    full_grid = get_side_fill_right_occlusion(full_grid, step)
    full_grid = get_side_fill_left_occlusion(full_grid, step)
    full_grid = get_back_fill_occlusion(full_grid, step)
    full_grid = get_roof_right_occlusion(full_grid, step)
    full_grid = get_roof_left_occlusion(full_grid, step)
    full_grid = get_back_occlusion(full_grid, step)
    full_grid = get_roof_occlusion(full_grid, step)
    # critical indexes of points on the cars surface are equal to 0
    critical_index = np.zeros((len(full_grid), 1))
    full_grid = np.hstack((full_grid, critical_index)) / 1000.005
    return full_grid

# functions to add to full grid coordinates of points on different regions from occlusion_geometry dataframe;
# step - difference of x, y or z coordinates of two nearest points of some region;
# grid - two-dimensional numpy array with 3 columns (x, y, z coordinates of points) and
# n rows (number of points which belong to one region);
# full_grid - two-dimensional numpy array with 3 columns (x, y, z coordinates of points) and
# n rows (number of points which belong to all regions);
# functions return full_grid


# Right side
def get_right_side_occlusion(step):
    x = np.linspace(0, -4922, int(4922/step))
    y = 1000
    z = np.linspace(0, 1200, int(1000/step))
    xs, ys, zs = np.meshgrid(x, y, z)
    xs, ys, zs = xs[zs < 1000 - xs / 7], ys[zs < 1000 - xs / 7], zs[zs < 1000 - xs / 7]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = grid
    return full_grid


# Left side
def get_left_side_occlusion(full_grid, step):
    x = np.linspace(0, -4922, int(4922/step))
    y = -1000
    z = np.linspace(0, 1200, int(1000/step))
    xs, ys, zs = np.meshgrid(x, y, z)
    xs, ys, zs = xs[zs < 1000 - xs / 7], ys[zs < 1000 - xs / 7], zs[zs < 1000 - xs / 7]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Front
def get_front_occlusion(full_grid, step):
    y = np.linspace(-1000, 1000, int(2000/step))
    x = 0
    z = np.linspace(0, 1000, int(1000/step))
    xs, ys, zs = np.meshgrid(x, y, z)
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Back
def get_back_occlusion(full_grid, step):
    y = np.linspace(-1000, 1000, int(2000/step))
    x = -4922
    z = np.linspace(0, 1200, int(1000/step))
    xs, ys, zs = np.meshgrid(x, y, z)
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Windshield
def get_windshield_occlusion(full_grid, step):
    x = np.linspace(-1400, -2000, int(np.sqrt(400**2 + 600**2) / step))
    y = np.linspace(-1000, 1000, int(2000/step))
    xs, ys = np.meshgrid(x, y)
    zs = 267 - 2 * xs / 3

    xs, ys, zs = xs[(zs < 1.14 * ys + 2342) & (zs < - 1.14 * ys + 2342)],\
                 ys[(zs < 1.14 * ys + 2342) & (zs < - 1.14 * ys + 2342)],\
                 zs[(zs < 1.14 * ys + 2342) & (zs < - 1.14 * ys + 2342)]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Boonet
def get_bonnet_occlusion(full_grid, step):
    x = np.linspace(0, -1400, int(np.sqrt(1400**2 + 200**2)/step))
    y = np.linspace(-1000, 1000, int(2000/step))
    xs, ys = np.meshgrid(x, y)
    zs = 1000 - xs / 7
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Front Roof
def get_front_roof_occlusion(full_grid, step):
    x = np.linspace(-2000, -2200, int(np.sqrt(145**2 + 200**2)/step))
    y = np.linspace(-650, 650, int(1300/step))
    xs, ys = np.meshgrid(x, y)
    zs = 150 - xs * 0.725
    xs, ys, zs = xs[(zs < 1.45 * ys + 2542.5) & (zs < - 1.45 * ys + 2542.5)],\
                 ys[(zs < 1.45 * ys + 2542.5) & (zs < - 1.45 * ys + 2542.5)],\
                 zs[(zs < 1.45 * ys + 2542.5) & (zs < - 1.45 * ys + 2542.5)]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Side Fill Right
def get_side_fill_right_occlusion(full_grid, step):
    x = np.linspace(-4922, -1400, int(3522/step))
    y = np.linspace(1000, 650, int(np.sqrt(350**2 + 400**2)/step))
    xs, ys = np.meshgrid(x, y)
    zs = -1.142 * ys + 2342.85
    xs, ys, zs = xs[(zs < - 2 * xs / 3 + 266.667) & (zs < 0.766 * xs + 4971.647)],\
                 ys[(zs < - 2 * xs / 3 + 266.667) & (zs < 0.766 * xs + 4971.647)],\
                 zs[(zs < - 2 * xs / 3 + 266.667) & (zs < 0.766 * xs + 4971.647)]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Side Fill Left
def get_side_fill_left_occlusion(full_grid, step):
    x = np.linspace(-4922, -1400, int(3522/step))
    y = np.linspace(-1000, -650, int(np.sqrt(350**2 + 400**2)/step))
    xs, ys = np.meshgrid(x, y)
    zs = 1.142 * ys + 2342.85
    xs, ys, zs = xs[(zs < - 2 * xs / 3 + 266.667) & (zs < 0.766 * xs + 4971.647)],\
                 ys[(zs < - 2 * xs / 3 + 266.667) & (zs < 0.766 * xs + 4971.647)],\
                 zs[(zs < - 2 * xs / 3 + 266.667) & (zs < 0.766 * xs + 4971.647)]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Back Fill
def get_back_fill_occlusion(full_grid, step):
    x = np.linspace(-4922, -4400, int(np.sqrt(522**2 + 400**2)/step))
    y = np.linspace(-1000, 1000, int(2000/step))
    xs, ys = np.meshgrid(x, y)
    zs = 0.766 * xs + 4971.648
    xs, ys, zs = xs[(zs < - 1.142 * ys + 2342.857) & (zs < 1.142 * ys + 2342.857)],\
                 ys[(zs < - 1.142 * ys + 2342.857) & (zs < 1.142 * ys + 2342.857)],\
                 zs[(zs < - 1.142 * ys + 2342.857) & (zs < 1.142 * ys + 2342.857)]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Roof Right
def get_roof_right_occlusion(full_grid, step):
    x = np.linspace(-4400, -2000, int(2400/step))
    y = np.linspace(-650, -550, int(np.sqrt(100**2 + 145**2)/step))
    xs, ys = np.meshgrid(x, y)
    zs = 1.45 * ys + 2542.5
    xs, ys, zs = xs[(zs < -0.725 * xs + 150.0)], ys[(zs < -0.725 * xs + 150.0)], zs[(zs < -0.725 * xs + 150.0)]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Roof Left
def get_roof_left_occlusion(full_grid, step):
    x = np.linspace(-4400, -2000, int(2400/step))
    y = np.linspace(650, 550, int(np.sqrt(100**2 + 145**2)/step))
    xs, ys = np.meshgrid(x, y)
    zs = - 1.45 * ys + 2542.5
    xs, ys, zs = xs[(zs < -0.725 * xs + 150.0)], ys[(zs < -0.725 * xs + 150.0)], zs[(zs < -0.725 * xs + 150.0)]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Back roof
def back_roof_occlusion(full_grid, step):
    x = -4400
    y = np.linspace(-650, 650, int(1300/step))
    z = np.linspace(1600, 1745, int(345/step))
    xs, ys, zs = np.meshgrid(x, y, z)
    xs, ys, zs = xs[(zs < 1.45 * ys + 2542.5) & (zs < - 1.45 * ys + 2542.5)],\
                 ys[(zs < 1.45 * ys + 2542.5) & (zs < - 1.45 * ys + 2542.5)], \
                 zs[(zs < 1.45 * ys + 2542.5) & (zs < - 1.45 * ys + 2542.5)]
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid


# Roof
def get_roof_occlusion(full_grid, step):
    x = np.linspace(-4400, -2200, int(2200/step))
    y = np.linspace(550, -550, int(1100/step))
    z = 1745
    xs, ys, zs = np.meshgrid(x, y, z)
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten(),), axis=-1)
    full_grid = np.concatenate((full_grid, grid))
    return full_grid
