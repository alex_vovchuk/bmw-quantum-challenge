import numpy as np


def cartesian_to_cylindrical(x, y, z):
    """Convert Cartesian coordinates x, y, z to cylindrical coordinates rho, phi, h"""
    r = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    h = z
    return r, phi, h


def cartesian_to_spherical(x, y, z):
    """Convert Cartesian coordinates x, y, z to spherical coordinates rho, phi, theta"""
    r = np.sqrt(np.power(x, 2) + np.power(y, 2) + np.power(z, 2))
    phi = np.degrees(np.arctan2(y, x))
    theta = np.degrees(np.arctan2(np.sqrt(np.power(x, 2) + np.power(y, 2)), z))
    grid = np.stack((r, phi, theta), axis=-1)
    return grid
