from Modules.import_data import get_data
import numpy as np
from math import ceil


# dataframes with data from BMW webpage
regions_of_interest_cubic, regions_of_interest_sector, allowed_sensor_positions = get_data()


# function to get coordinates of areas on car where sensor of some type can be placed
# from dataframe with allowed positions for all sensors types;
# sensor - string type of sensor (camera, ultrasound, radar or lidar);
# returns dictionary where keys are names of rectangle areas on car where sensor can be placed,
# values - two-dimensional numpy arrays with 3 columns (x, y, z Cartesian coordinates of vertices for this areas) and
# 4 rows (number of vertices for each area)
def get_sensor_position(sensor):
    # create boolean list,
    # if element in mask with some index i is True, this type of sensor can be placed on the cars area
    # with the same index i in dataframe allowed_sensors_positions,
    # if element is equal to False, sensor can't be places in such area
    mask = []
    for row in allowed_sensor_positions.index:
        if sensor in allowed_sensor_positions['Allowed Sensors'][row]:
            mask.append(True)
        else:
            mask.append(False)
    # create dataframe which contains information from allowed_sensors_positions only about those areas
    # where sensor of specific type can be placed
    allowed_pos = allowed_sensor_positions[mask]
    # create dictionary where keys are names of areas and values are numpy arrays with coordinates for this area
    polygons = get_allowed_regions(allowed_pos)

    return polygons


def build_position_grid(sensor_type, step):
    # get dictionary with coordinates of vertices for rectangle areas on the car where sensor can be placed
    polygons = get_sensor_position(sensor_type)
    full_grid = {}
    step = 1000 * step
    for part, polygon in polygons.items():
        # polygon - numpy array with coordinates
        # part - names of regions on car

        # areas parallel to y-axis;
        # regions: front, bonnet, side mirror (left, right), camera behind windshield, front roof, back, trunk
        if polygon[0, 0] == polygon[1, 0]:
            # get array with coordinates of points on polygon
            grid = build_x_orient(polygon, step)
        # areas parallel to x-axis;
        # regions: side (left, right), pillar 1 and 2 (left, right), roof (left, right)
        elif polygon[0, 1] == polygon[1, 1]:
            # get array with coordinates of points on polygon
            grid = build_y_orient(polygon, step)
        # add polygons points to full grid
        full_grid['{}'.format(part)] = grid
    return full_grid


# function to get coordinates of points on areas which are parallel with y-axis and on which sensors can be placed;
# step - difference in x, y or z-coordinates of two nearest points on cars area where sensor can be placed;
# polygon - numpy array with coordinates of vertices of some area on the car;
# returns two-dimensional numpy array with 3 columns (x, y, z coordinates of points) and
# n rows (number of points on the polygon)
def build_x_orient(polygon, step):
    # for areas which are parallel to YOZ-plane (x1 = x2 = x3);
    # regions: front, back, trunk, side mirror (left, right)
    if polygon[0, 0] == polygon[2, 0]:
        x = polygon[0, 0]
        # number of different y-coordinates: (y_max - y_min) / step
        y = np.linspace(polygon[0, 1], polygon[1, 1], ceil(abs(polygon[0, 1] - polygon[1, 1]) / step))
        z = np.linspace(polygon[1, 2], polygon[2, 2], ceil(abs(polygon[1, 2] - polygon[2, 2]) / step))
        xs, ys, zs = np.meshgrid(x, y, z)
    # for areas, where x1 = x2 != x3 and y2 = y3;
    # regions: bonnet, camera behind windshield
    elif polygon[1, 1] == polygon[2, 1]:
        # number of different x-coordinates: sqrt((x_max - x_min)**2 + (z_max - z_min)**2) / step
        x = np.linspace(polygon[0, 0], polygon[2, 0], ceil(np.sqrt((polygon[0, 0] - polygon[2, 0]) ** 2
                                                                   + (polygon[1, 2] - polygon[2, 2]) ** 2) / step))
        # number of different y-coordinates: (y_max - y_min) / step
        y = np.linspace(polygon[0, 1], polygon[1, 1], ceil(abs(polygon[0, 1] - polygon[1, 1]) / step))
        xs, ys = np.meshgrid(x, y)
        # get coefficients k and b for line z = k * x + b from two points (x1, z1) and (x3, z3)
        k, b = equation_coof(polygon[0, 0], polygon[2, 0], polygon[0, 2], polygon[2, 2])
        zs = k * xs + b
    # for areas, where x1 = x2 != x3 and y2 != y3;
    # regions: front roof
    else:
        # number of different x-coordinates: sqrt((x_max - x_min)**2 + (z_max - z_min)**2) / step
        x = np.linspace(polygon[0, 0], polygon[2, 0], ceil(np.sqrt((polygon[0, 0] - polygon[2, 0]) ** 2
                                                                   + (polygon[1, 2] - polygon[2, 2]) ** 2) / step))
        # number of different y-coordinates: (y_max - y_min) / step
        y = np.linspace(polygon[0, 1], polygon[1, 1], ceil(abs(polygon[0, 1] - polygon[1, 1]) / step))
        xs, ys = np.meshgrid(x, y)
        # get coefficients k and b for line z = k * x + b from two points (x1, z1) and (x3, z3)
        k, b = equation_coof(polygon[0, 0], polygon[2, 0], polygon[0, 2], polygon[2, 2])
        zs = k * xs + b
        # get coefficients k1 and b1 for line z = k1 * y + b1 from two points (y2, z2) and (y3, z3)
        k1, b1 = equation_coof(polygon[1, 1], polygon[2, 1], polygon[1, 2], polygon[2, 2])
        xs, ys, zs = xs[(zs < k1 * ys + b1) & (zs < - k1 * ys + b1)], ys[(zs < k1 * ys + b1) & (zs < - k1 * ys + b1)], \
                     zs[(zs < k1 * ys + b1) & (zs < - k1 * ys + b1)]
    # get array with coordinates of points
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    return grid / 1000


# function to get coordinates of points on areas which are parallel with x-axis and on which sensors can be placed;
# step - difference in x, y or z-coordinates of two nearest points on cars area where sensor can be placed;
# polygon - numpy array with coordinates of vertices of some area on the car;
# returns two-dimensional numpy array with 3 columns (x, y, z coordinates of points) and
# n rows (number of points on the polygon)
def build_y_orient(polygon, step):
    # for areas which are parallel to plane XOZ (y1 = y2 = y3)
    # regions: side left, side right
    if polygon[1, 1] == polygon[2, 1]:
        # number of different x-coordinates: (x_max - x_min) / step
        x = np.linspace(polygon[0, 0], polygon[1, 0], ceil(abs(polygon[0, 0] - polygon[1, 0]) / step))
        y = polygon[1, 1]
        # number of different z-coordinates: (z_max - z_min) / step
        z = np.linspace(polygon[1, 2], polygon[2, 2], ceil(abs(polygon[1, 2] - polygon[2, 2]) / step))
        xs, ys, zs = np.meshgrid(x, y, z)
    # for areas where y1 = y2 != y3 and x2 = x3
    # regions: pillar left (1, 2), pillar right (1, 2)
    elif polygon[1, 0] == polygon[2, 0]:
        # number of different x-coordinates: (x_max - x_min) / step
        x = np.linspace(polygon[0, 0], polygon[2, 0], ceil(abs(polygon[0, 0] - polygon[1, 0]) / step))
        # number of different y-coordinates: sqrt((y_max - y_min)**2 + (z_max - z_min)**2) / step
        y = np.linspace(polygon[1, 1], polygon[2, 1], ceil(np.sqrt((polygon[1, 1] - polygon[2, 1]) ** 2
                                                                   + (polygon[1, 2] - polygon[2, 2]) ** 2) / step))
        xs, ys = np.meshgrid(x, y)
        # get coefficients k and b for line z = k * y + b from two points (y2, z2) and (y3, z3)
        k, b = equation_coof(polygon[1, 1], polygon[2, 1], polygon[1, 2], polygon[2, 2])
        zs = k * ys + b
    # for areas where y1 = y2 != y3 and x2 != x3
    # regions: roof left, roof right
    else:
        # number of different x-coordinates: (x_max - x_min) / step
        x = np.linspace(polygon[0, 0], polygon[1, 0], ceil(abs(polygon[0, 0] - polygon[1, 0]) / step))
        # number of different y-coordinates: sqrt((y_max - y_min)**2 + (z_max - z_min)**2) / step
        y = np.linspace(polygon[1, 1], polygon[2, 1], ceil(np.sqrt((polygon[1, 1] - polygon[2, 1]) ** 2
                                                                   + (polygon[1, 2] - polygon[2, 2]) ** 2) / step))
        xs, ys = np.meshgrid(x, y)
        # get coefficients k and b for line y = k * x + b from two points (x2, y2) and (x3, y3)
        k, b = equation_coof(polygon[1, 1], polygon[2, 1], polygon[1, 2], polygon[2, 2])
        zs = k * ys + b
        # get coefficients k1 and b1 for line z = k * x + b from two points (x2, z2) and (x3, z3)
        k1, b1 = equation_coof(polygon[1, 0], polygon[2, 0], polygon[1, 2], polygon[2, 2])
        xs, ys, zs = xs[zs < k1 * xs + b1], ys[zs < k1 * xs + b1], zs[zs < k1 * xs + b1]
    # get array with coordinates of points
    grid = np.stack((xs.flatten(), ys.flatten(), zs.flatten()), axis=-1)
    return grid / 1000


# function to find equation of line from two points (x1, z1) and (x2, z3);
# returns k and b coefficients in equation: k * x + b = z
def equation_coof(x1, x2, z1, z2):
    k = ((z1 - z2) / (x1 - x2))
    b = -k * x2 + z2
    return k, b


# function to get coordinates of vertices of rectangle areas where sensor can be placed;
# allowed_pos - dataframe with data about allowed sensors positions
# returns dictionary where keys are names of rectangle areas on car where sensor can be placed,
# values - two-dimensional numpy arrays with 3 columns (x, y, z Cartesian coordinates of vertices for this areas) and
# 4 rows (number of vertices for each area)
def get_allowed_regions(allowed_pos):
    polygons = {}

    for position in range(len(allowed_pos.index)):
        # create list [[x1, x2, x3, x4], [y1, y2, y3, y4], [z1, z2, z3, z4]],
        # where x1, y1, z1, ... - values of cells in row with index equal to position and
        # in columns with names x1, y1, z1, ... respectively
        i = [[allowed_pos.iloc[position, j + 3 * i] for i in range(0, 4)] for j in range(1, 4)]
        # create numpy arrays from each row of list
        x = np.array([*i[0]])  # i[0] list of x-coordinates
        y = np.array([*i[1]])  # i[1] list of y-coordinates
        z = np.array([*i[2]])  # i[2] list of z-coordinates
        # create list of lists with three columns which correspond to x, y and z coordinates:
        # grid[i][0] - x-coordinates, grid[i][1] - y-coordinates, grid[i][2] - z-coordinates
        grid = np.stack((x, y, z), axis=-1)
        # create dictionary where keys are names of areas and values are numpy arrays with coordinates for this area
        polygons['{}'.format(allowed_pos['Region'].iloc[position])] = grid

    return polygons
