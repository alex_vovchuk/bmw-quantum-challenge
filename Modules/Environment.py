import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from Modules.Sensor import Sensor
from Modules.import_data import get_data
from Modules.helper_functions import cartesian_to_cylindrical, cartesian_to_spherical
from Modules.Occlusion_geometry import get_occlusion


# Class Environment with parameters:
# scenario - string type of scenario (Parking", "Lane keep", "Traffic sign recognition", etc. for one type of scenario);
# roi_type - string type of regions of interest (cubic or sector);
# sensors - list with objects of class Sensor;
# grid_step - step of grid (projection of distance between two nearest greed points on x, y, or z axis);
# grid - numpy array with 4 columns (x, y, and z (for cubic RoI) or r, phi and h (for sector RoI) coordinates of points
# of environment and value of critical index at these points) and n rows (n - number of points which belong to
# environments region with certain general and additional name of scenario);
# vehicle - two-dimensional numpy array with 4 columns (x, y, z coordinates of points on cars surface and values of
# critical index (always 0) at these points) and n rows (number of points which belong to all regions of cars surface);
# full_grid - grid with vehicle: numpy array with n + m rows (n - number of points in grids for cubic or sector RoI;
# m - number of points in vehicles grid) and 4 columns (x, y, and z (for cubic RoI) and value of critical index at these
# points; or r, phi and h (for sector RoI) coordinates of points of environment and critical index at these points;
# or x, y, z coordinates of points on cars surface and critical indexes at these points). For some functions in this
# class some parts of full_grid (vehicle part or grid part) may be absent;
# budget - total budget;
# sensors - list of objects of class Sensor;
class Environment:
    # create dataframes with necessary data
    regions_of_interest_cubic, regions_of_interest_sector, allowed_sensor_positions = get_data()

    # function to create environment without sensors;
    # function sets values of some environments parameters and builds full grid for certain scenario using data about
    # cubic and sector regions of interest;

    def __init__(self, scenario, budget=0, roi_type='Cubic', grid_step=0.2):
        self.scenario = scenario
        self.roi_type = roi_type
        self.sensors = []
        self.grid_step = grid_step
        self.budget = budget

        # Making full grid for one kind of scenario
        # for cubic regions of interest
        grid = np.array([[0, 0, 0, 0]])
        if self.roi_type == 'Cubic':
            # create dataframe with data about cubic regions of interest for specific scenario from environment
            scenario_data = self.regions_of_interest_cubic.loc[[self.scenario]]

            # scenario_data similarly to regions_of_interest_cubic has two indexes (general names of scenarios and
            # additional names of scenarios)
            # get data from all rows in scenario_data with certain general name of scenario and different additional
            # scenarios names
            for index in scenario_data.index.get_level_values(1):
                # grid - numpy array with 4 columns (x, y and z coordinates of points of environment and critical index
                # at these points) and n rows (n - number of points which belong to environments region with certain
                # general and additional name of scenario)
                grid = np.concatenate((grid, self.build_cub_grid(index)))
            # remove row with coordinates of first point (0, 0, 0) and zero critical index
            grid = grid[1:]
        # for sector regions of interest
        elif self.roi_type == 'Sector':
            # create dataframe with data about sector regions of interest for specific scenario from environment
            scenario_data = self.regions_of_interest_sector.loc[[self.scenario]]

            # get data from all rows in scenario_data with certain general name of scenario and different additional
            # scenarios names
            for index in scenario_data.index.get_level_values(1):
                # grid - numpy array with 4 columns (r, phi and h coordinates of points of environment and critical
                # index at these points) and n rows (n - number of points which belong to environments region with
                # certain general and additional name of scenario)
                grid = np.concatenate((grid, self.build_sector_grid(index)))
            # remove row with coordinates of first point (0, 0, 0) and zero critical index
            grid = grid[1:]
        # combine cubic or sector grid with vehicle grid
        self.full_grid = np.concatenate((grid, get_occlusion(self.grid_step)))

    # function to print information about environment
    # prints type of scenario
    def __repr__(self):
        return 'Environment for {} scenario'.format(self.scenario)

    # function to add grids for different scenarios;
    # other - object of class Environment with another type of scenario;
    # returns object of class Environment
    def __add__(self, other):
        # add grid of environment and grid of other-environment;
        grid = np.concatenate((self.full_grid, other.full_grid))
        # take unique points (including same points with different critical index)
        grid = np.unique(grid, axis=0)
        # take unique points (without critical index)
        _, indexes = np.unique(grid[:, :-1], axis=0, return_inverse=True)

        # replace values of critical indexes for points with the same coordinates and different critical indexes with
        # maximum value of critical index among these points
        copies = []
        for i, j in enumerate(indexes):
            if j in indexes[:i]:
                copies.append([i, np.where(indexes[:i] == j)[0][-1]])
        if copies:
            for pair in copies:
                c_indexes = [grid[pair[0]][-1:], grid[pair[1]][-1:]]
                c_index = np.max(c_indexes)
                grid[pair[0]][-1:] = c_index
                grid[pair[1]][-1:] = c_index

        # Take unique points and create new grid
        grid = np.unique(grid, axis=0)
        self.full_grid = np.concatenate((grid, get_occlusion(self.grid_step)))

        # Adding sensors from both scenario`s
        self.sensors = self.sensors + other.sensors
        list_of_sensors = self.sensors
        self.remove_all_sensors()
        for index, sensor in enumerate(list_of_sensors):
            self.add_sensor(sensor, index)
        # Adding scenarios
        self.scenario = self.scenario + '({})'.format(self.roi_type) + ', ' + other.scenario + '({})'\
            .format(other.roi_type)

        # Add budget from different scenarios
        self.budget = self.budget + other.budget

        return self

    # function to build environment grid with preset step from cubic RoI
    # index - second index in scenario_data dataframe (corresponds to additional names of scenario for certain
    # general name of scenario)
    # returns numpy array with 4 columns (x, y and z coordinates of points of environment and value of critical index
    # at these points) and n rows (n - number of points which belong to environments region with certain general and
    # additional name of scenario)
    def build_cub_grid(self, index):
        # get grid step
        step = self.grid_step
        # get coordinates of start- and end- points of rectangle region for certain general and additional names
        # of scenario;
        # create dataframe with data about cubic regions of interest for specific scenario from environment
        scenario_data = self.regions_of_interest_cubic.loc[[self.scenario]]
        x1 = scenario_data.loc[(self.scenario, index)]['start_point_x']
        x2 = scenario_data.loc[(self.scenario, index)]['end_point_x']
        y1 = scenario_data.loc[(self.scenario, index)]['start_point_y']
        y2 = scenario_data.loc[(self.scenario, index)]['end_point_y']
        z1 = scenario_data.loc[(self.scenario, index)]['start_point_z']
        z2 = scenario_data.loc[(self.scenario, index)]['end_point_z']

        # create numpy arrays with x, y and z coordinates of points on grid of environment for certain scenario type
        max_x = max(x1, x2)
        min_x = min(x1, x2)
        x = np.arange(min_x, max_x + step, step)
        x = [round(i, 1) for i in x]

        max_y = max(y1, y2)
        min_y = min(y1, y2)
        y = np.arange(min_y, max_y + step, step)
        y = [round(i, 1) for i in y]

        max_z = max(z1, z2)
        min_z = min(z1, z2)
        z = np.arange(min_z, max_z + step, step)
        z = [round(i, 1) for i in z]

        # create arrays with the same length for x, y and z coordinates
        xs, ys, zs = np.meshgrid(x, y, z)
        xs_grid = xs.flatten()
        ys_grid = ys.flatten()
        zs_grid = zs.flatten()

        # get critical index of environments regions for certain general and additional names af scenario
        c_index = scenario_data.loc[(self.scenario, index)]['critical_index']
        # create numpy array filled with values of critical index and with the length equal to length of arrays
        # with x, y and z coordinates
        critical_index = np.full_like(xs_grid, fill_value=c_index)
        # create numpy array with 4 columns (x, y and z coordinates of points of environment and critical index at this
        # point) and n rows (n - number of points)
        grid = np.stack((xs_grid, ys_grid, zs_grid, critical_index), axis=-1)
        return grid

    # function to build environment grid with preset step from sector RoI
    # index - second index in scenario_data dataframe (corresponds to additional names of scenario for certain
    # general name of scenario)
    # returns numpy array with 4 columns (r, phi and h coordinates of points of environment and critical index at these
    # points) and n rows (n - number of points which belong to environments region with certain general and additional
    # name of scenario)
    def build_sector_grid(self, index):
        # get grid step
        step = self.grid_step
        # get x, y, z coordinates of the center, maximum and minimum radius`s of sector region for certain general
        # and additional names of scenario
        # create dataframe with data about sector regions of interest for specific scenario from environment
        scenario_data = self.regions_of_interest_sector.loc[[self.scenario]]

        center = (scenario_data.loc[(self.scenario, index)]['centre_x'],
                  scenario_data.loc[(self.scenario, index)]['centre_y'],
                  scenario_data.loc[(self.scenario, index)]['centre_z'])
        r_min = scenario_data.loc[(self.scenario, index)]['radius_min']
        r_max = scenario_data.loc[(self.scenario, index)]['radius max']

        # get values of two angles phi_min and phi_max in cylindrical coordinate system;
        # sector region is located between these angles
        phi_min = (scenario_data.loc[(self.scenario, index)]['yaw_angle']
                   - scenario_data.loc[(self.scenario, index)]['h_angle'] / 2) * np.pi / 180
        phi_max = (scenario_data.loc[(self.scenario, index)]['yaw_angle']
                   + scenario_data.loc[(self.scenario, index)]['h_angle'] / 2) * np.pi / 180
        # get height of the region
        height = scenario_data.loc[(self.scenario, index)]['height']

        # create arrays with x, y and z coordinates of points of parallelepiped with sides 2 * r_max, 2 * r_max, h;
        # center[0], center[1], center[2] - x, y, z coordinates of sectors center
        x = np.arange(center[0] - r_max, center[0] + r_max + step, step)
        x = [round(i, 1) for i in x]

        y = np.arange(center[1] - r_max, center[1] + r_max + step, step)
        y = [round(i, 1) for i in y]

        z = np.arange(center[2], center[2] + height + step, step)
        z = [round(i, 1) for i in z]

        # create numpy arrays of the same length for x, y and z coordinates of parallelograms points
        xs, ys, zs = np.meshgrid(x, y, z)
        xs_grid = xs.flatten()
        ys_grid = ys.flatten()
        zs_grid = zs.flatten()
        # create arrays of the same length for x, y and z coordinates of points relatively to sectors center
        x_r = xs_grid - center[0]
        y_r = ys_grid - center[1]
        z_r = zs_grid - center[2]

        # create arrays with spherical coordinates of parallelograms points relatively to sectors center
        r, phi, h = cartesian_to_cylindrical(x_r, y_r, z_r)

        # mask_phi - boolean numpy array;
        # value of mask_phi at index i is True only when value of phi at the same index i is between phi_min and phi_max
        mask_phi = (phi <= phi_max) & (phi >= phi_min)
        mask_r = (r >= r_min) & (r <= r_max)
        # combine two masks
        # value of mask at index i is True only when value of phi at the same index i is between phi_min and phi_max and
        # when value of r at index i is between r_max and r_min
        mask = mask_r & mask_phi

        # get critical index of environments regions for certain general and additional names af scenario
        c_index = scenario_data.loc[(self.scenario, index)]['critical_index']
        # create numpy array filled with values of critical index and with the length equal to length of arrays
        # with r, phi and h coordinates of parallelograms points
        critical_index = np.full_like(xs_grid, fill_value=c_index)
        # create numpy array with 4 columns (r, phi and h coordinates of points of environment and critical index at
        # these points) and n rows (n - number of points)
        grid = np.stack((xs_grid[mask], ys_grid[mask], zs_grid[mask], critical_index[mask]), axis=-1)

        return grid

    # function to create new sensor;
    # s_type - string type of sensor;
    # pos - array with x, y, z coordinates of sensors position;
    # orient - array with spherical coordinates (phi, theta) of points which represent sensors orientation;
    # function returns object of class Sensor with given parameters
    @staticmethod
    def init_sensor(s_type, pos=0, orient=0, polygon=None):
        sensor = Sensor(pos, orient, s_type)
        # polygon - specific part of allowed sensor position of sensor type (optional)
        sensor.polygon = polygon
        return sensor

    # function to create boolean mask of sensor in particular environment and add object Sensor to self.sensors;
    # sensor - object of class Sensor, which will be added;
    # index - position of object Sensor in list self.sensors;
    def add_sensor(self, sensor, index):
        # Creating full grid with center in position of Sensor;
        grid = self.full_grid
        x = grid[:, 0] - sensor.position[0]
        y = grid[:, 1] - sensor.position[1]
        z = grid[:, 2] - sensor.position[2]

        # transition to spherical coordinates;
        spherical_grid = cartesian_to_spherical(x, y, z)
        r, phi, theta = spherical_grid[:, 0], spherical_grid[:, 1], spherical_grid[:, 2]

        # Creating mask of sensor field of view;
        # For angle phi taken into account situation, when angle of view cross the border (180; -180) degrees;
        if sensor.orientation[0] + sensor.horizontal_angle / 2 >= 180:
            mask_phi = (phi >= sensor.orientation[0] - sensor.horizontal_angle / 2) | (
                        phi <= sensor.orientation[0] + sensor.horizontal_angle / 2 - 360)
        elif sensor.orientation[0] - sensor.horizontal_angle / 2 <= - 180:
            mask_phi = (phi <= sensor.orientation[0] + sensor.horizontal_angle / 2) | (
                        phi >= (sensor.orientation[0] - sensor.horizontal_angle / 2 + 360))
        else:
            mask_phi = (phi <= sensor.orientation[0] + sensor.horizontal_angle / 2) & (
                        phi >= sensor.orientation[0] - sensor.horizontal_angle / 2)

        # Creating mask for theta;
        mask_theta = (theta >= sensor.orientation[1] - sensor.vertical_angle / 2) &\
                     (theta <= sensor.orientation[1] + sensor.vertical_angle / 2)
        # Creating mask for r:
        mask_r = r <= sensor.s_range

        # Adding all mask;
        sensor_mask = mask_r & mask_phi & mask_theta
        vehicle_mask = (grid[:, 3] == 0) & sensor_mask       # Creating mask for part of vehicle in sensor field of view
        final_mask = np.full_like(sensor_mask, fill_value=0, dtype=bool)

        # Evaluating of occlusion;
        if len(grid[vehicle_mask]) > 0:
            # Take points of grid, which get into sensor field of view;
            vehicle_points = grid[vehicle_mask]

            # Creating grid in spherical coordinates and with center in sensor position;
            xs = vehicle_points[:, 0] - sensor.position[0]
            ys = vehicle_points[:, 1] - sensor.position[1]
            zs = vehicle_points[:, 2] - sensor.position[2]
            vehicle_points_sph = cartesian_to_spherical(xs, ys, zs)
            error = 1    # Add angle error;
            # Find maximum and minimum theta in which have points of vehicle;
            min_theta = min(vehicle_points_sph[:, 2]) - 2 * error
            max_theta = max(vehicle_points_sph[:, 2]) + 2 * error
            num_of_delta = int(max_theta - min_theta) // 2

            # For slice with delta_theta angle find points behind vehicle;
            for delta_theta in np.linspace(min_theta, max_theta, num_of_delta):
                # Creating mask for one slice;
                delta_mask = (theta >= delta_theta - error) & (theta <= delta_theta + (max_theta - min_theta)
                                                               / num_of_delta + error) & sensor_mask & (r >= 0.4)
                if len(grid[delta_mask & vehicle_mask]) > 0:
                    # Find maximum and minimum of phi in one slice and take mean of r;
                    min_phi = min(phi[delta_mask & vehicle_mask]) - error
                    max_phi = max(phi[delta_mask & vehicle_mask]) + error
                    r_mean = np.mean(r[delta_mask & vehicle_mask])
                    # Creating mask for points behind vehicle;
                    all_occlusion_mask = (phi >= min_phi) & (phi <= max_phi) & (r >= r_mean) & (theta >= delta_theta) &\
                                         (theta <= delta_theta + 3)
                    # Subtract found points;
                    final_mask = final_mask | all_occlusion_mask
        # Adding attribute "mask" to object Sensor;
        sensor.mask = sensor_mask & ~final_mask & ~vehicle_mask
        # Adding object Sensor to list of environment sensors;
        self.sensors.insert(index, sensor)

    # Function to create standard sensor position state for quantum algorithm
    # n - number of sensors (from 1 to 8)
    def sensor_setup(self, n):
        list_of_sensors = [Sensor([-3.7, -1., 1], [-90, 90], s_type='Ultrasound'),
                           Sensor([0., 0., 1], [0, 90], s_type='Ultrasound'),
                           Sensor([-3.7, -1., 1], [-90, 90], s_type='Ultrasound'),
                           Sensor([-3.7, 1., 1], [90, 90], s_type='Ultrasound'),
                           Sensor([-3.7, 1., 1], [90, 90], s_type='Ultrasound'),
                           Sensor([-5., -1., 1], [-180, 90], s_type='Ultrasound'),
                           Sensor([-5., 1., 1], [-180, 90], s_type='Ultrasound'),
                           Sensor([0, -1., 1], [0, 90], s_type='Ultrasound')]
        for i in range(n):
            self.add_sensor(list_of_sensors[i], i)

    # function to remove sensor from list of all sensors in environment;
    # sensor - object of class Sensor
    def remove_sensor(self, sensor):
        if sensor in self.sensors:
            self.sensors.remove(sensor)

    # function to remove all sensors in environment
    def remove_all_sensors(self):
        self.sensors = []

    # function to rotate sensor on certain angle;
    # new_orientation - list of spherical coordinates phi and theta;
    # sensor - object of class Sensor;
    # function removes sensor with old values of spherical coordinates which represent its orientation and
    # creates new sensor with new values from new_orientation
    def rotate_sensor(self, sensor, new_orientation):
        if sensor in self.sensors:
            index = self.sensors.index(sensor)
            self.remove_sensor(sensor)
            sensor.change_orientation(new_orientation)
            self.add_sensor(sensor, index)

    # function to move sensor on certain angle;
    # sensor - object of class Sensor;
    # new_orientation - numpy array with Cartesian coordinates of sensor;
    # function removes sensor with old values of coordinates and creates new sensor with new values from new_position;
    def change_position_of_sensor(self, sensor, new_position):
        if sensor in self.sensors:
            index = self.sensors.index(sensor)
            self.remove_sensor(sensor)
            sensor.change_position(new_position)
            self.add_sensor(sensor, index)

    # Plot environment grid with all sensors masks, vehicle and allowed positions
    def plot_grid(self):
        fig = plt.figure(figsize=(14, 8), dpi=80)
        ax = fig.add_subplot(projection='3d')

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        ax.set_title(self.scenario)

        # Plot Environment grid
        plot = ax.scatter(self.full_grid[:, 0][self.full_grid[:, 3] != 0], self.full_grid[:, 1][self.full_grid[:, 3] != 0],
                          self.full_grid[:, 2][self.full_grid[:, 3] != 0], s=1, c=self.full_grid[:, 3][self.full_grid[:, 3] != 0],
                          alpha=0.2, cmap=cm.jet)
        fig.colorbar(plot, shrink=0.5, aspect=5)
        ax.set_zlim3d(0, 5)
        # Plot sensor masks
        if len(self.sensors) > 0:
            # colors = itertools.cycle(["r", "b", "g"])
            for sensor in self.sensors:
                ax.scatter(self.full_grid[:, 0][sensor.mask], self.full_grid[:, 1][sensor.mask],
                           self.full_grid[:, 2][sensor.mask], s=3, color='yellow')
                for polygon in sensor.allowed_position.values():
                    ax.scatter(polygon[:, 0], polygon[:, 1], polygon[:, 2], s=3, c='black')
            ax.view_init(90, 0)
        return plt.show()

    # Function to represents sensor setup.
    def print_setup(self):
        print('Environment created by scenario {}'.format(self.scenario))
        print('(Budget: {} $. Grid step: {})'.format(self.budget, self.grid_step))
        print('{:-^80}'.format('List of presents sensors'))
        print('{:-^20}'.format('Sensor type') + '{:-^25}'.format('Position [x, y, z]')
              + '{:-^20}'.format('Orientation [phi, theta]') + '{:-^15}'.format('Price'))
        prices = 0
        for sensor in self.sensors:
            x, y, z = sensor.position
            coord = [round(x, 2), round(y, 2), round(z, 2)]
            print('{:^20}'.format(str(sensor.type)) + '{:^25}'.format(str(coord))
                  + '{:^20}'.format(str(sensor.orientation)) + '{:^22}'.format(str(sensor.price)))
            prices += sensor.price
        print('Total cost: {} $'.format(prices))
