import numpy as np
import itertools
import time
from math import floor


# cost function with parameters:
# environment - object of class Environment;
class CostFunction:
    # Hardcoded prices for sensors
    prices = {'Ultrasound': 25., 'Camera': 70., 'Radar': 100., 'Lidar': 1500.}

    # create cost function
    # env - object of class Environment;
    def __init__(self, env):
        self.environment = env

    # Evaluation of covered volume  of all sensors in environment
    def covered_volume(self):
        # full_grid - numpy arrays with 4 columns (x, y, z and value of critical index at these points, which contains
        # grid of RoI and grid of vehicle for occlusion geometry) with n rows (n - number of points in full grid);
        # mask - boolean numpy array with full_grid size which contains information about cover of each point (True -
        # point covered by any sensor, False - point don`t covered by any sensor);
        # sensors - list of sensors in the environment

        # create empty mask with full_grid size:
        null_mask = np.full_like(self.environment.full_grid[:, 0], fill_value=False)
        # add masks of all sensors which are in the environment
        for sensor in self.environment.sensors:
            null_mask = np.append(null_mask, sensor.mask)
        # create new 2-dimensional array where each column represents all mask values for certain sensor
        masks = null_mask.reshape((len(self.environment.sensors) + 1, len(self.environment.full_grid[:, 0]))).T
        # aggregating to the mask
        mask = np.any(masks, axis=1)
        # count number of all points covered by any sensor
        volume = np.sum(self.environment.full_grid[:, 3][mask])
        return volume

    # Evaluation of ratio of covered to uncovered volume, normalized by budget
    def get_effective_volume(self):
        return self.covered_volume() / np.sum(self.environment.full_grid[:, 3]) * self.environment.budget

    # Evaluation of ratio of remaining of budget to all budget
    def get_effective_cost(self):
        types = self.check_sensor_type()
        price = 0
        for sensor_type, n in types.items():
            price += self.prices[sensor_type] * n
        if price > self.environment.budget:
            eff_cost = np.inf
        else:
            eff_cost = (self.environment.budget - price) / self.environment.budget
        return eff_cost

    # Count numbers of each type of sensor
    # Return dictionary with keys - types and values - number of sensors each type
    def check_sensor_type(self):
        types = dict()
        for sensor in self.environment.sensors:
            if sensor.type in types:
                types[sensor.type] += 1
            else:
                types[sensor.type] = 1
        return types

    # Evaluation of cost function
    def get_cost(self):
        if not self.environment.sensors:
            return 0
        else:
            cost = - self.get_effective_volume() + self.get_effective_cost()
        return cost


# Creating parts of allowed zones for check in which direction sensor look
    part = {'Front': ['Front', 'Bonnet', 'Side Mirror Left', 'Side Mirror Right', 'Camera Behind Windshield',
                      'Front Roof'], 'Back': ['Back', 'Trunk'], 'Left': ['Side Left', 'Pillar Left 1', 'Pillar Left 2',
                                                                         'Roof Left'],
            'Right': ['Side Right', 'Pillar Right 1', 'Pillar Right 2', 'Roof Right']}

    # Function for evaluating all possible dispositions for specific sensor
    # num_of_phi - number of phi (horizontal angle) which sensor can be oriented
    # num_of_theta - number of theta (vertical angle) which sensor can be oriented
    # sensor_type - list of sensor types which can be placed
    # return nested list with 4 columns (position of sensors, orientation of sensor, type of sensor and mask of sensor)
    # and m * n * l rows (n - all possible orientation, m - all possible position, l - all possible types)
    def get_dispositions(self, num_of_phi, num_of_theta, sensor_types):

        # Saving sensors of environment and removing them
        present_sens = self.environment.sensors
        self.environment.remove_all_sensors()

        # Initializing list for return
        all_type_cov = []

        # Choose specific sensor type
        for s_type in sensor_types:
            print('\nEvaluating coverage for all possible positions/orientations for {}...'.format(s_type))
            sensor = self.environment.init_sensor(s_type)
            full_cov = []
            # Choose polygon (specific rectangle areas of allowed zone)
            for index, polygon in enumerate(sensor.allowed_position.keys()):
                print('Calculating coverage for all possible positions/orientations in {} polygon ({}/{})'
                      .format(polygon, index + 1, len(sensor.allowed_position.keys())))
                start = time.time()
                if polygon in self.part['Front']:
                    angles = self.get_angles(sensor.horizontal_angle, sensor.vertical_angle, num_of_phi, num_of_theta,
                                             'Front')
                elif polygon in self.part['Back']:
                    angles = self.get_angles(sensor.horizontal_angle, sensor.vertical_angle, num_of_phi, num_of_theta,
                                             'Back')
                elif polygon in self.part['Left']:
                    angles = self.get_angles(sensor.horizontal_angle, sensor.vertical_angle, num_of_phi, num_of_theta,
                                             'Left')
                else:
                    angles = self.get_angles(sensor.horizontal_angle, sensor.vertical_angle, num_of_phi, num_of_theta,
                                             'Right')
                pos = np.array(sensor.allowed_position[polygon])

                # Evaluating of possible masks for all positions in polygon and all orientations
                polygon_cov = []
                for pos, angles in itertools.product(pos, angles):
                    row = []
                    sensor = self.environment.init_sensor(s_type, pos, angles)
                    self.environment.add_sensor(sensor, 0)
                    mask = sensor.mask

                    # Adding results of evaluating for final answer
                    row.append(pos)
                    row.append(angles)
                    row.append(s_type)
                    row.append(mask)
                    polygon_cov.append(row)

                    # Rollback to empty environment
                    self.environment.remove_all_sensors()

                # Adding results for all points of polygon
                full_cov.extend(polygon_cov)
                # Print time spent in loop
                end = time.time()
                print('Time spent {}.'.format(time.strftime("%H:%M:%S", time.gmtime(end - start))))

            # Adding results for all polygon of one type
            all_type_cov.extend(full_cov)

        # Back to initial state
        self.environment.sensors = present_sens
        return all_type_cov

    # Function to evaluating coefficients for Hamiltonian, which represents weight of each sensor in environment in each
    # position and orientation;
    # num_of_orient - integer, number of possible orientations of phi;
    # sensors_type - list with type of sensors
    # Return dictionary, where keys - bitstring, that represent angle of rotations, and values - numpy array with
    # effective covers, length n, were n - number of sensors.
    def get_weights(self, num_of_orient, sensors_type):
        type_dict = dict()          # Create dictionary for all types
        # Create list of initial types
        init_types = []
        for sens in self.environment.sensors:
            s_type = sens.type
            init_types.append(s_type)
        start_init_types = init_types.copy()
        for s_type in sensors_type:
            dict_of_j = dict()          # Create dictionary with coverage areas for one type of sensor
            sensor = self.environment.init_sensor(s_type)
            angle_step = (180. - sensor.horizontal_angle) / (num_of_orient - 1.)  # Field of view divided by parts
            angles = []
            for i in range(num_of_orient):  # Creating list of possible rotations
                angle = i * angle_step
                angles.append(angle)
            len_of_str = len(bin(num_of_orient)[2:])  # Length of position/orientation string
            dict_of_j['0'.zfill(len_of_str)] = np.zeros(len(self.environment.sensors))
            # Create list of initial orientation
            init_orient = []
            for sens in self.environment.sensors:
                init_orient.append(sens.orientation)
            # Loop for angles
            for alpha in angles:
                list_of_sensor = []
                # Put each sensor in particular angle and choose correspond type
                for num_of_sensor in range(len(self.environment.sensors)):
                    if self.environment.sensors[num_of_sensor].type != s_type:
                        old_sensor = self.environment.sensors[num_of_sensor]
                        pos, orient = old_sensor.position, old_sensor.orientation
                        sensor = self.environment.init_sensor(s_type, pos=pos, orient=orient)
                        self.environment.remove_sensor(old_sensor)
                        self.environment.add_sensor(sensor, num_of_sensor)
                    else:
                        sensor = self.environment.sensors[num_of_sensor]
                    self.environment.rotate_sensor(sensor, [init_orient[num_of_sensor][0] - 90 + sensor.horizontal_angle
                                                            / 2 + alpha,
                                                            init_orient[num_of_sensor][1]])
                    # Calculate relative coverage
                    j = np.sum(self.environment.full_grid[:, 3][self.environment.sensors[num_of_sensor].mask]) /\
                        np.sum(self.environment.full_grid[:, 3])
                    j = round(j, 4)  # Round coverage to prevent occlusion error
                    list_of_sensor.append(j)

                # Writing results to dict with key - binary represent of orientation
                a = bin(int(alpha // floor(angle_step)) + 1)[2:].zfill(len_of_str)
                dict_of_j[a] = np.array(list_of_sensor)
            # Return environment to initial state
            for index, sens in enumerate(self.environment.sensors):
                self.environment.rotate_sensor(sens, init_orient[index])
            # Dictionary with all type of sensors coverage
            type_dict[s_type] = dict_of_j
        # Return to initial types
        for index, sens in enumerate(self.environment.sensors):
            pos, orient = sens.position, sens.orientation
            sensor = self.environment.init_sensor(start_init_types[index], pos=pos, orient=orient)
            self.environment.remove_sensor(sens)
            self.environment.add_sensor(sensor, index)
        return type_dict

    # Function to evaluating coefficients for Hamiltonian, which represents weight of overlaps for each pair of sensors
    # in each position and orientation;
    # num_of_orient - integer, number of possible orientations of phi;
    # sensors_type - list 0ith type of sensors
    # Return dictionary, where keys - bitstring, that represent angles of rotations of i- and j-sensors, and values -
    # numpy array with effective overlap`s covers with shape NxN, were N - number of sensors.
    def get_overlap_weights(self, num_of_orient, sensors_type):
        type_dict = dict()          # Create dictionary for all types
        # Create list of initial types
        init_types = []
        for sens in self.environment.sensors:
            s_type = sens.type
            init_types.append(s_type)
        start_init_types = init_types.copy()
        for s_type1, s_type2 in itertools.combinations_with_replacement(sensors_type, 2):
            dict_of_j = dict()    # Create final dictionary with overlaps

            len_of_str = len(bin(num_of_orient)[2:])    # Length of one orientation string
            # Create list of initial orientation and types
            init_orient = []
            init_types = []
            for sens in self.environment.sensors:
                init_orient.append(sens.orientation)
                init_types.append(sens.type)

            # Evaluating of overlaps for sensors and angles pairs
            alphas, betas, angle_step1, angle_step2 = self.get_overlaps_angles(num_of_orient, s_type1, s_type2)
            for alpha, beta in itertools.product(alphas, betas):
                matrix = []
                for first_sensor in range(len(self.environment.sensors)):
                    row = []
                    for second_sensor in range(len(self.environment.sensors)):
                        if first_sensor == second_sensor:
                            row.append(0.)
                        elif first_sensor != second_sensor:
                            # Choose first sensor and rotate it
                            sens_1 = self.prepare_sens(first_sensor, s_type1, init_orient, alpha)

                            # Choose second sensor and rotate it
                            sens_2 = self.prepare_sens(second_sensor, s_type2, init_orient, beta)

                            # Evaluating of overlap
                            j = np.sum(self.environment.full_grid[:, 3][self.environment.sensors[first_sensor].mask &
                                                                        self.environment.sensors[second_sensor].mask]) \
                                / np.sum(self.environment.full_grid[:, 3])
                            j = round(j, 4)    # Round overlap to prevent occlusion error
                            row.append(j)

                            # Rotate sensors to initial orientations
                            self.environment.rotate_sensor(sens_1, init_orient[first_sensor])
                            self.environment.rotate_sensor(sens_2, init_orient[second_sensor])
                    matrix.append(row)
                # Creating binary string which represent orientation of sensors
                a = bin(int(alpha // floor(angle_step1)) + 1)[2:].zfill(len_of_str)
                b = bin(int(beta // floor(angle_step2)) + 1)[2:].zfill(len_of_str)
                dict_of_j[a + b] = np.array(matrix)
            type_dict[s_type1 + '/' + s_type2] = dict_of_j
            for index, sensor in enumerate(self.environment.sensors):
                if sensor.type == init_types[index]:
                    pass
                else:
                    sens = self.environment.sensors[index]
                    self.environment.remove_sensor(sens)
                    sensor = self.environment.init_sensor(sens.type, sens.position, sens.orientation)
                    self.environment.add_sensor(sensor, index)
        # Return to initial types
        for index, sens in enumerate(self.environment.sensors):
            pos, orient = sens.position, sens.orientation
            sensor = self.environment.init_sensor(start_init_types[index], pos=pos, orient=orient)
            self.environment.remove_sensor(sens)
            self.environment.add_sensor(sensor, index)
        return type_dict

    # Function to create angles for overlaps
    # return possible angles and angle`s steps
    def get_overlaps_angles(self, num_of_orient, s_type1, s_type2):
        # Initializing sensors
        sensor1 = self.environment.init_sensor(s_type1)
        sensor2 = self.environment.init_sensor(s_type2)
        # Finding angles for each type of sensors
        angle_step1 = (180. - sensor1.horizontal_angle) / (num_of_orient - 1.)  # Field of view divided by parts
        angle_step2 = (180. - sensor2.horizontal_angle) / (num_of_orient - 1.)  # Field of view divided by parts

        # Creating list of possible rotations for first sensor
        alphas = []
        for i in range(num_of_orient):
            angle = i * angle_step1
            alphas.append(angle)
        # Creating list of possible rotations for second sensor
        betas = []
        for i in range(num_of_orient):
            angle = i * angle_step2
            betas.append(angle)
        return alphas, betas, angle_step1, angle_step2

    # Function for preparing and rotating sensors
    def prepare_sens(self, index, s_type, init_orient, angle):
        # Choose sensor
        sens = self.environment.sensors[index]
        # Check sensor type
        if sens.type == s_type:
            pass
        else:
            self.environment.remove_sensor(sens)
            sensor = self.environment.init_sensor(s_type, sens.position, sens.orientation)
            self.environment.add_sensor(sensor, index)
            sens = self.environment.sensors[index]
        # Rotate sensor
        self.environment.rotate_sensor(sens, [init_orient[index][0] - 90 + sens.horizontal_angle / 2 + + angle,
                                              init_orient[index][1]])

        return sens

    # Function, which create all pairs of all possible angles of sensors
    # s_h - horizontal angle of sensor`s view
    # s_v - - vertical angle of sensor`s view
    # num_of_phi - number of phi (horizontal angle) which sensor can be oriented
    # num_of_theta - number of theta (vertical angle) which sensor can be oriented
    # part - parts of allowed zones for check in which direction sensor can look
    # Returns itertools.product() of all possible angle
    @staticmethod
    def get_angles(s_h, s_v, num_of_phi, num_of_theta, part):
        if part == 'Front':
            start_angle = - 90
        elif part == 'Back':
            start_angle = 90
        elif part == 'Left':
            start_angle = 0
        else:
            start_angle = - 180
        phis = []
        if num_of_phi == 1:
            phis = [start_angle + 90]
        else:
            phi_angle_step = (180. - s_h) / (num_of_phi - 1.)
            for i in range(num_of_phi):    # Creating list of possible phis
                phi = (start_angle + s_h / 2) + i * phi_angle_step
                if phi > 180:
                    phi = phi - 360
                elif phi < -180:
                    phi = phi + 360
                phis.append(phi)
        thetas = []
        if num_of_theta == 1:
            thetas = [90]
        else:
            theta_angle_step = (180. - s_v) / (num_of_theta - 1.)
            for i in range(num_of_theta):  # Creating list of possible theta
                theta = s_v / 2 + i * theta_angle_step
                thetas.append(theta)
        pairs = itertools.product(phis, thetas)
        return pairs
