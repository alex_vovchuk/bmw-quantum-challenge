import pandas as pd
from zipfile import ZipFile
from io import BytesIO
import urllib.request as urllib2


# function to get data from BMW resource;
# returns three dataframes with cubic regions of interest, sector regions of interest and allowed sensors positions
def get_data():
    file = get_zip_file()
    regions_of_interest_cubic = get_cubic_roi(file)
    regions_of_interest_sector = get_sector_roi(file)
    allowed_sensor_positions = get_allowed_positions(file)
    return regions_of_interest_cubic, regions_of_interest_sector, allowed_sensor_positions


def get_zip_file():
    # upload zip.file with such files in "Sensor position" folder: "allowed_sensor_positions.csv",
    # "critically_grid_0_5.csv", "occlusion_geometry.csv",
    # "regions_of_interest_cubic.csv", "regions_of_interest.sector.csv"
    r = urllib2.urlopen(
        "https://crowd-innovation.bmwgroup.com/apps/IMT/UploadedFiles/00"
        "/f_424a1e36e809bcf39d5d5f6291b33182/Data-Sensor_position.zip?v=1626652188").read()
    return ZipFile(BytesIO(r))


# function to create dataframe with cubic RoI data and split its indexes on general and additional names; of scenarios
# returns this dataframe
def get_cubic_roi(file):
    # creating dataframe with cubic RoI data
    csv_1 = file.open(r"Sensor_position/regions_of_interest_cubic.csv")
    regions_of_interest_cubic = pd.read_csv(csv_1, sep=';+',
                                            engine='python')

    # Structuring data according to scenarios
    cub_scenarios = []  # general names of scenarios
    cub_parts = []  # additional names of scenarios
    for row in regions_of_interest_cubic['label']:
        # row - name of scenario;
        # if name of scenario has "-" symbol, we assume that first part of row (before "-") is general name of scenario
        # and second part (after "-") is its additional name,
        # if row doesn't contain this symbol, scenario has only general name - full row
        indexes = [x.strip() for x in row.split('-')]
        cub_scenarios.append(indexes[0])
        cub_parts.append(indexes[1])
    # set new indexes in dataframe
    regions_of_interest_cubic.index = [cub_scenarios, cub_parts]
    return regions_of_interest_cubic


# print names of all cubic scenarios
def print_cubic_scenarios(cub_scenarios):
    cub_scen_set = set(cub_scenarios)
    a = "Scenarios with cubic region: "
    for i in range(len(cub_scen_set)):
        a = a + '{}, '
    a = a[:-2] + '.\n'
    print(a.format(*cub_scen_set))


# function to create dataframe with sector RoI data and split its indexes on general and additional names of scenarios
# returns this dataframe
def get_sector_roi(file):
    # create dtaframe with sector RoI data
    csv_2 = file.open(r"Sensor_position/regions_of_interest_sector.csv")
    regions_of_interest_sector = pd.read_csv(csv_2, sep=';+', engine='python')

    # Structuring data
    sec_scenarios = []  # genral names of scenarios
    sec_parts = []  # additional names of scenarios
    for row in regions_of_interest_sector['label']:
        # row - name of scenario;
        # if name of scenario has "-" symbol, we assume that first part of row (before "-") is general name of scenario
        # and second part (after "-") is its additional name,
        # if row doesn't contain this symbol, scenario has only general name - full row
        indexes = [x.strip() for x in row.split('-')]
        sec_scenarios.append(indexes[0])
        sec_parts.append(indexes[1])
    # set new indexes in dataframe
    regions_of_interest_sector.index = [sec_scenarios, sec_parts]
    return regions_of_interest_sector


# print names of all sector scenarios
def print_sector_scenarios(sec_scenarios):
    sector_scen_set = set(sec_scenarios)
    b = "Scenarios with sector region: "
    for i in range(len(sector_scen_set)):
        b = b + '{}, '
    b = b[:-2] + '.'  # string with all general names of sector scenarios
    print(b.format(*sector_scen_set))


# function to create dataframe with allowed sensor positions data;
# returns this dataframe
def get_allowed_positions(file):
    # create dataframe with allowed sensor position data
    csv_3 = file.open(r"Sensor_position/allowed_sensor_positions.csv")
    allowed_sensor_positions = pd.read_csv(csv_3, sep=';+', engine='python')
    # remove empty columns
    allowed_sensor_positions.dropna(axis='columns', how='all', inplace=True)
    return allowed_sensor_positions
