# BMW Quantum Challenge

Repository for BMW Quantum Computing Challenge: Sensor Postions Optimization. 

## Structure of repository
All code related to problem solving contained in branch > master. All available feature of results presented in Jupyter Notebook `main`. Branch > quantum_algorithm contained additional theoretical information to describe the Hamiltonian, Ising interaction and sensor representation.

## Source code
Source code is divided into two parts: `Modules` and `Optimization`. 

### Modules
Folder `Modules` contains all code needed to construct the `Environment` for calculations. The `Environment` contains all functions needed for creating, plotting, and communicating with other modules. The `Sensor` is corresponding to the object `Sensor` and all manipulation with it. `Occlusion_geometry`, `import_data`, `allowed_sensor_possition` are needed to import and process data.

### Optimization
`Optimization` consists two part: `Classical_algorithms` and `Quantum_algorithms`. Classical_algorithms contains realization of Greedy algorithm for Budgeted Max Coverage Problem. In Quantum_algorithms we have all code related to QAOA and estimation of ground state.

All code commented and user-frendly.
